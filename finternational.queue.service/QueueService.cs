using Azure.Messaging.ServiceBus;
using finternational.queue.service.Configuration;
using finternational_common.Messaging;
using finternational_common.Persistence.Interfaces;
using Newtonsoft.Json;
using System.Text;

namespace finternational.queue.service
{
    public class QueueService : BackgroundService
    {
        private readonly ILogger<QueueService> _logger;
        private readonly IUnitOfWorkProvider unitOfWorkProvider;
        private readonly PersistenceSettings persistenceSettings;
        private readonly QueueSettings queueSettings;
        private const int numberOfMessages = 50;
        private readonly ServiceBusClient client;

        public QueueService(
            ILogger<QueueService> logger, 
            IUnitOfWorkProvider unitOfWorkProvider, 
            PersistenceSettings persistenceSettings,
            QueueSettings queueSettings)
        {
            this.unitOfWorkProvider = unitOfWorkProvider;
            this.persistenceSettings = persistenceSettings;
            this.queueSettings = queueSettings;
            client = new ServiceBusClient(queueSettings.ConnectionString);
            _logger = logger;
        }

        

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                using(var unitOfWork = this.unitOfWorkProvider.Create(persistenceSettings.ConnectionString))
                {
                    var messages = await unitOfWork.MessageRepository.GetUnprocessedMessages();
                    var messagesToSend = messages.Take(numberOfMessages).ToList();

                    _logger.LogInformation($"Found {messages.Count()} unprocessed messages, batching {messagesToSend.Count()}");
                    
                    if(messagesToSend.Count > 0)
                    {
                        var sender = client.CreateSender(queueSettings.QueueName);

                        using (ServiceBusMessageBatch messageBatch = await sender.CreateMessageBatchAsync())
                        {
                            foreach (var message in messagesToSend)
                            {
                                var finternationalMessage = new FinternationalMessage(message.Type, message.Data, message.Id);

                                if (!messageBatch.TryAddMessage(new ServiceBusMessage(Encoding.Default.GetBytes(JsonConvert.SerializeObject(finternationalMessage)))))
                                {
                                    throw new Exception($"The messageid: {message.Id} is too large to fit in the batch.");
                                }
                            }

                            try
                            {
                                await sender.SendMessagesAsync(messageBatch);          

                                await unitOfWork.MessageRepository.SetMessageBatchAsProcessed(messagesToSend.Select(x => x.Id).ToList());
                                await unitOfWork.Save();

                                _logger.LogInformation($"A batch of {messagesToSend.Count()} messages has been published to the queue.");
                            }
                            finally
                            {
                                await sender.DisposeAsync();
                            }
                        }
                    }
                }

                await Task.Delay(1000, stoppingToken);
            }

            await client.DisposeAsync();
        }
    }
}