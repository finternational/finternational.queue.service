using finternational.queue.service;
using finternational.queue.service.Configuration;

IHost host = Host.CreateDefaultBuilder(args)
    .ConfigureServices((hostContext, services) =>
    {
        IConfiguration configuration = hostContext.Configuration;

        // Add services to the container.
        services.RegisterPersistenceSettings(configuration);
        services.RegisterQueueSettings(configuration);
        services.AddPersistenceLayer();
        services.AddHostedService<QueueService>();
    })
    .Build();

await host.RunAsync();
