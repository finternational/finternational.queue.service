﻿using finternational_common.Persistence;
using finternational_common.Persistence.Interfaces;

namespace finternational.queue.service.Configuration
{
    public static class PersistenceConfigurationExtensions
    {
        public static void AddPersistenceLayer(this IServiceCollection services)
        {
            services.AddTransient<IUnitOfWorkProvider, UnitOfWorkProvider>();
            services.AddTransient<IConnectionProvider, ConnectionProvider>();
            services.AddTransient<ICommandExecutor, CommandExecutor>();
            services.AddTransient<IQueryExecutor, QueryExecutor>();
        }
    }
}
