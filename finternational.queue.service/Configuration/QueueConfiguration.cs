﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace finternational.queue.service.Configuration
{
    public record QueueSettings(string ConnectionString, string QueueName) { }
    public static class QueueConfiguration
    {
        public static void RegisterQueueSettings(this IServiceCollection services, IConfiguration config)
        {
            services.AddSingleton(new QueueSettings(
                config.GetValue<string>("Queue:ConnectionString"),
                config.GetValue<string>("Queue:QueueName")));
        }
    }
}
