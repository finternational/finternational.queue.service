﻿namespace finternational.queue.service.Configuration
{
    public record PersistenceSettings(string ConnectionString){}
    public static class PersistenceConfiguration
    {
        public static void RegisterPersistenceSettings(this IServiceCollection services, IConfiguration config)
        {
            services.AddSingleton(new PersistenceSettings(
                config.GetValue<string>("Persistence:ConnectionString")));
        }
    }
}
