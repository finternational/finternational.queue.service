FROM mcr.microsoft.com/dotnet/sdk:6.0 AS builder
WORKDIR /src
EXPOSE 80
COPY . .

#Restore Dependencies
RUN dotnet restore "finternational.queue.service/finternational.queue.service.csproj" -s "https://www.nuget.org/api/v2/" -s "https://gitlab.com/api/v4/projects/35958452/packages/nuget/index.json"

#Publish App
RUN dotnet publish "finternational.queue.service/finternational.queue.service.csproj" -c Release -o /app/publish
FROM mcr.microsoft.com/dotnet/sdk:6.0  AS final
#Copy App and Run
WORKDIR /app
COPY --from=builder /app/publish /app
ENTRYPOINT ["dotnet", "finternational.queue.service.dll"]
